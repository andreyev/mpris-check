cmake_minimum_required(VERSION 3.14)

project(mpris-check VERSION 0.1 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# skip Qt6 for now
find_package(QT NAMES Qt5 REQUIRED COMPONENTS Core Quick)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core Quick)

message("Qt version: ${QT_VERSION_MAJOR}")

find_package(QCoro5 COMPONENTS Core DBus QUIET)
if(NOT QCoro5_FOUND)
    find_package(QCoro REQUIRED)
endif()
qcoro_enable_coroutines()

set(PROJECT_SOURCES
        main.cpp
        mprischecker.cpp
        qml.qrc
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(mpris-check
        MANUAL_FINALIZATION
        ${PROJECT_SOURCES}
    )
# Define target properties for Android with Qt 6 as:
#    set_property(TARGET mpris-check APPEND PROPERTY QT_ANDROID_PACKAGE_SOURCE_DIR
#                 ${CMAKE_CURRENT_SOURCE_DIR}/android)
# For more information, see https://doc.qt.io/qt-6/qt-add-executable.html#target-creation
else()
    if(ANDROID)
        add_library(mpris-check SHARED
            ${PROJECT_SOURCES}
        )
# Define properties for Android with Qt 5 after find_package() calls as:
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    else()
        add_executable(mpris-check
          ${PROJECT_SOURCES}
        )
    endif()
endif()

target_compile_definitions(mpris-check
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)

qt5_add_dbus_interface(mpris_srcs "org.mpris.MediaPlayer2.Player.xml" mprisplayerinterface)
target_sources(mpris-check PRIVATE ${mpris_srcs})

if (TARGET QCoro5::Core)
    target_link_libraries(mpris-check PRIVATE
        Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Quick

        QCoro::Core
        QCoro::DBus
    )
else()
    target_link_libraries(mpris-check PRIVATE
        Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Quick

        QCoro::QCoroCore
        QCoro::QCoroDBus
    )
endif()

set_target_properties(mpris-check PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

if(QT_VERSION_MAJOR EQUAL 6)
    qt_import_qml_plugins(mpris-check)
    qt_finalize_executable(mpris-check)
endif()
