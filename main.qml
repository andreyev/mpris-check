import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("MPRIS Checker")

    RowLayout {
        anchors.fill: parent
        anchors.margins: 20

        Button {
            Layout.fillWidth: true
            text: qsTr("Pause")
            onClicked: {
                console.log("Pause")
                mprisChecker.pauseMedia()
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Unpause")
            onClicked: {
                console.log("Unpause")
                mprisChecker.unpauseMedia()
            }
        }
    }
}
