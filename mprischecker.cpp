#include "mprischecker.h"

#include <QDBusConnection>
#include <QDebug>

#include "mprisplayerinterface.h"

MprisChecker::MprisChecker(QObject *parent)
    : QObject{parent}
{

}

void MprisChecker::check()
{
    qDebug() << "check";

}

void MprisChecker::pauseMedia()
{
    const QStringList interfaces = QDBusConnection::sessionBus().interface()->registeredServiceNames().value();
    for (const QString &iface : interfaces) {
        if (iface.startsWith(QLatin1String("org.mpris.MediaPlayer2"))) {
            if (iface.contains(QLatin1String("kdeconnect"))) {
                qDebug() << Q_FUNC_INFO << "Skip players connected over KDE Connect. KDE Connect pauses them itself";
                continue;
            }
            qDebug() << Q_FUNC_INFO << "Found player:" << iface;

            org::mpris::MediaPlayer2::Player mprisInterface(iface, QStringLiteral("/org/mpris/MediaPlayer2"), QDBusConnection::sessionBus());
            QString status = mprisInterface.playbackStatus();
            qDebug() << Q_FUNC_INFO << "Found player status:" << iface << status;
            if (status == QLatin1String("Playing")) {
                if (!_pausedSources.contains(iface)) {
                    _pausedSources.insert(iface);
                    if (mprisInterface.canPause()) {
                        mprisInterface.Pause();
                    } else {
                        mprisInterface.Stop();
                    }
                }
            }
        }
    }
}

void MprisChecker::unpauseMedia()
{
    for (const QString &iface : qAsConst(_pausedSources)) {
        org::mpris::MediaPlayer2::Player mprisInterface(iface, QStringLiteral("/org/mpris/MediaPlayer2"), QDBusConnection::sessionBus());
        mprisInterface.Play();
    }
    _pausedSources.clear();
}
