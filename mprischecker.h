#ifndef MPRISCHECKER_H
#define MPRISCHECKER_H

#include <QObject>
#include <qqml.h>

class MprisChecker : public QObject
{
    Q_OBJECT
public:
    explicit MprisChecker(QObject *parent = nullptr);

Q_SIGNALS:

public Q_SLOTS:
    void check();
    void pauseMedia();
    void unpauseMedia();

private:
    QSet<QString> _pausedSources;
};

#endif // MPRISCHECKER_H
